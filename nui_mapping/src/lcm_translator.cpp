#include "lcm_translator.h"

NUI_MAPPING_NS_HEAD

void LCMTranslator::Params::fromServer(ros::NodeHandlePtr node) {
  node->param<std::string>("lcm/navstate_channel", lcm.navstate_channel, "NAVSTATE");
  node->param<std::string>("ros/odom_topic", ros.odom_topic, "odom");
  node->param<std::string>("ros/odom_frame", ros.odom_frame, "odom");
  node->param<std::string>("ros/odom_body_frame", ros.odom_body_frame, "base_link");
  node->param("ros/interp_rate", ros.interp_rate, 5.0);
}

LCMTranslator::LCMTranslator() {
  ros_node_ptr_.reset(new ros::NodeHandle("~"));
  params.fromServer(ros_node_ptr_);
  // setup lcm
  lcm_.subscribe(params.lcm.navstate_channel, &LCMTranslator::navstateHandler, this);
  // setup ros
  pubs_.odom = ros_node_ptr_->advertise<nav_msgs::Odometry>(params.ros.odom_topic,100);

  latest_interp_time_ = -1;

  for (size_t i = 0; i<NUM_POS_FIELDS; i++) {
    last_time[i] = -1;
  }

  timer_ = ros_node_ptr_->createTimer(ros::Duration(5), &LCMTranslator::timerCallback, this);
}

void LCMTranslator::spinOnce(){
  lcm_.handleTimeout(100);
  ros::spinOnce();
}

void LCMTranslator::spin(){
  ros::Rate loop_rate(200); // in hz
  while (ros::ok()) {
    spinOnce();
    loop_rate.sleep();
  }
}


bool LCMTranslator::allFieldsUpdated() {
  bool out = true;
  for (size_t i = 0; i<NUM_POS_FIELDS; i++) {
    out = out && pos_updated[i];
  }
  return out;
}

void LCMTranslator::interpAndSend(const sensors::navState_t *msg) {
  double interp_period = 1.0/params.ros.interp_rate;

  _1D::LinearInterpolator<double> interp[NUM_POS_FIELDS];
  for (size_t i = 0; i<NUM_POS_FIELDS; i++) {
    interp[i].setData(pos_times[i], pos_batch[i]);
  }

  // pack odom message
  for ( ; latest_interp_time_ < latest_complete_time_; latest_interp_time_+= interp_period) {
    nav_msgs::Odometry odom_msg;

    odom_msg.header.stamp.fromSec(latest_interp_time_);

    odom_msg.header.frame_id = params.ros.odom_frame;
    odom_msg.child_frame_id  = params.ros.odom_body_frame;

    odom_msg.pose.pose.position.x = interp[0](latest_interp_time_);
    odom_msg.pose.pose.position.y = interp[1](latest_interp_time_);
    odom_msg.pose.pose.position.z = interp[2](latest_interp_time_);

//    tf2::Quaternion qbody2ned, ned_orientation;
//    qbody2ned.setRPY(interp[5](latest_interp_time_),interp[4](latest_interp_time_),interp[3](latest_interp_time_));

//    tf2::Matrix3x3 ned2enu = tf2::Matrix3x3(0,1,0,1,0,0,0,0,-1);
//    tf2::Quaternion q_ned2enu;
//    ned2enu.getRotation(q_ned2enu);

    tf2::Quaternion quat( interp[QUAT_X](latest_interp_time_),
                          interp[QUAT_Y](latest_interp_time_),
                          interp[QUAT_Z](latest_interp_time_),
                          interp[QUAT_W](latest_interp_time_));
    quat.normalize();
    odom_msg.pose.pose.orientation = tf2::toMsg(quat);


    geometry_msgs::TransformStamped transformStamped;

    transformStamped.header=odom_msg.header;
    transformStamped.child_frame_id = odom_msg.child_frame_id;

    transformStamped.transform.translation.x = odom_msg.pose.pose.position.x;
    transformStamped.transform.translation.y = odom_msg.pose.pose.position.y;
    transformStamped.transform.translation.z = odom_msg.pose.pose.position.z;

    transformStamped.transform.rotation.x = odom_msg.pose.pose.orientation.x;
    transformStamped.transform.rotation.y = odom_msg.pose.pose.orientation.y;
    transformStamped.transform.rotation.z = odom_msg.pose.pose.orientation.z;
    transformStamped.transform.rotation.w = odom_msg.pose.pose.orientation.w;

    pubs_.tf.sendTransform(transformStamped);
    pubs_.odom.publish(odom_msg);

    timer_.stop();
    timer_.start();
  }

  // trim buffer
  for (size_t i = 0; i<NUM_POS_FIELDS; i++) {
    //std::cout << pos_batch[i].size() << ",";
    pos_updated[i]=false;
    auto last_pos = pos_batch[i];
    auto last_pos_time = pos_times[i];
    pos_batch[i].clear();
    pos_times[i].clear();
    for (size_t j = 0; j<last_pos_time.size(); j++) {
      if(last_pos_time[j] >= (latest_interp_time_ - 3.0)) {
        pos_times[i].push_back(last_pos_time[j]);
        pos_batch[i].push_back(last_pos[j]);
      }
    }
  }
}

void LCMTranslator::navstateHandler(const lcm::ReceiveBuffer *rbuf, 
		                    const std::string &chan, 
				    const sensors::navState_t *msg) {
  ////  printf("Received message on channel \"%s\":\n", chan.c_str());
  // printf("  timestamp   = %lld\n", (long long)msg->utime);
  // printf("  position    = (%f, %f, %f, %f, %f, %f)\n",
  //       msg->pos[0], msg->pos[1], msg->pos[2], msg->pos[3], msg->pos[4], msg->pos[5]);
  //printf("  pos_time    = (%ld, %ld, %ld, %ld, %ld, %ld)\n",
  //       msg->pos_time[0], msg->pos_time[1], msg->pos_time[2], msg->pos_time[3], msg->pos_time[4], msg->pos_time[5]);
//  ////  printf("  pos_source    = (%d, %d, %d, %d, %d, %d)\n",
  ////          msg->pos_source[0], msg->pos_source[1], msg->pos_source[2], msg->pos_source[3], msg->pos_source[4], msg->pos_source[5]);

  last_msg = *msg;

  // fill out pos batch
  for (size_t i = 0; i<3; i++) {
    if(last_time[i] != msg->pos_time[i]) {
      pos_updated[i] = true;
      pos_batch[i].push_back(msg->pos[i]);
      pos_times[i].push_back(msg->pos_time[i]/1e6);
      last_time[i] = msg->pos_time[i];
    } else {
      // TODO(lindzey): Fix this hack -- it was added to allow testing on deck, 
      //     when x/y positions aren't updating.
      pos_updated[i] = true;
      pos_batch[i].push_back(msg->pos[i]);
      pos_times[i].push_back(msg->utime/1e6);
      last_time[i] = msg->utime;
    }
  }

  tf2::Quaternion qbody2ned, ned_orientation;
  qbody2ned.setRPY(msg->pos[5], msg->pos[4], msg->pos[3]);

  tf2::Matrix3x3 ned2enu = tf2::Matrix3x3(0,1,0,1,0,0,0,0,-1);
  tf2::Quaternion q_ned2enu;
  ned2enu.getRotation(q_ned2enu);

  auto quat = tf2::toMsg(q_ned2enu*qbody2ned);

  pos_batch[QUAT_X].push_back(quat.x);
  pos_batch[QUAT_Y].push_back(quat.y);
  pos_batch[QUAT_Z].push_back(quat.z);
  pos_batch[QUAT_W].push_back(quat.w);
  if (last_time[QUAT_X] != msg->pos_time[QUAT_X]) {  // these are all linked to yaw update
    for (size_t i = 3; i<NUM_POS_FIELDS; i++) {
        pos_updated[i] = true;
        pos_times[i].push_back(msg->pos_time[QUAT_X]/1e6);
        last_time[i] = msg->pos_time[QUAT_X];
    }
  }

  latest_complete_time_= pos_times[0].back();
  for (size_t i = 0; i<NUM_POS_FIELDS; i++) {
    latest_complete_time_ = std::min(latest_complete_time_, pos_times[i].back());
  }

  if (latest_interp_time_ <= 0) {
    latest_interp_time_ = latest_complete_time_;
  }

  if (allFieldsUpdated()) {
    bool ready = true;
    for (size_t i=0; i < NUM_POS_FIELDS; i++) {
      ready = ready && pos_batch[i].size()>1;
    }
    if(ready)
      interpAndSend(msg);
  }
}

void LCMTranslator::timerCallback(const ros::TimerEvent& e) {
  sensors::navState_t * msg = &last_msg;
  ROS_WARN("The LCM_bridge hasn't received a complete NAVSTATE position message in a while\n"
            "   make sure all 6 DOF of the position field are updating\n"
            "   this is the last message that was received:\n"
            "   position    = (%f, %f, %f, %f, %f, %f)\n"
            "   pos_time    = (%ld, %ld, %ld, %ld, %ld, %ld)\n",
           msg->pos[0], msg->pos[1], msg->pos[2], msg->pos[3], msg->pos[4], msg->pos[5],
           msg->pos_time[0], msg->pos_time[1], msg->pos_time[2], 
	   msg->pos_time[3], msg->pos_time[4], msg->pos_time[5]);
}


NUI_MAPPING_NS_FOOT
