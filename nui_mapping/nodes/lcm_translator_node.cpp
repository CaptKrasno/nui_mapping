#include "lcm_translator.h"

int main(int argc, char **argv)
{
  ros::init(argc, argv, "lcm_translator_node");
  nui_mapping::LCMTranslator translator;
  translator.spin();
}
