#ifndef DEFS_H
#define DEFS_H

#include <stddef.h>

///
///  Constants
///
#define NUI_MAPPING_PI                                                                 \
  3.1415926535897932384626433832795028841971693993751058209749445923078164062

#define POS_X 0
#define POS_Y 1
#define POS_Z 2
#define QUAT_X 3
#define QUAT_Y 4
#define QUAT_Z 5
#define QUAT_W 6
///
///  Namespace stuff
///
#define NUI_MAPPING_NS_HEAD namespace nui_mapping {

#define NUI_MAPPING_NS_FOOT }

#endif
