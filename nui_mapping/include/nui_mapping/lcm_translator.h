#ifndef TEMPLATE_CLASS_H
#define TEMPLATE_CLASS_H

#include <ros/ros.h>
#include <lcm/lcm-cpp.hpp>
#include "navState_t.hpp"
#include <nav_msgs/Odometry.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2_ros/transform_broadcaster.h>
#include "defs.h"
#include <cmath>
#include <libInterpolate/Interpolate.hpp>
#include <deque>

#define NUM_POS_FIELDS 7

NUI_MAPPING_NS_HEAD

struct DoubleTimePair{
  DoubleTimePair(double val, int64_t t){
    value = val;
    time = t * 1000000.0;
  }
  double value;
  double time;
};

class LCMTranslator
{
public:
  LCMTranslator();
  /*!
   * \brief spin is degined to run the node continuously
   */
  void spin();
  /*!
   * \brief spin_once is intended to run the loop just once
   */
  void spinOnce();
  /*!
   * \brief navstateHandler handles the incomming navstate LCM messages
   * \param rbuf
   * \param chan
   * \param msg
   */
  void navstateHandler(const lcm::ReceiveBuffer* rbuf,
          const std::string& chan,
          const sensors::navState_t* msg);

  void timerCallback(const ros::TimerEvent& e);

  /*!
   * \brief The Params struct describes the params for the LCMTranslator class
   */
  struct Params{
    struct{
      std::string navstate_channel;  //!<  the channel to listen for the navstate_channel
    }lcm;
    struct{
      std::string odom_topic;        //!<  the ros topic you want to publish odometry on
      std::string odom_frame;        //!<  the child
      std::string odom_body_frame;
      double interp_rate;
    }ros;
    void fromServer(ros::NodeHandlePtr node);
  };

protected:
  bool allFieldsUpdated();
  void interpAndSend(const sensors::navState_t *msg);
  ros::NodeHandlePtr ros_node_ptr_;
  lcm::LCM lcm_;
  struct{
    ros::Publisher odom;
    tf2_ros::TransformBroadcaster tf;
  } pubs_;
  Params params;

  double latest_complete_time_;
  double latest_interp_time_;
  int64_t last_time[NUM_POS_FIELDS];
  bool pos_updated[NUM_POS_FIELDS];
  std::vector<double> pos_batch[NUM_POS_FIELDS];
  std::vector<double> pos_times[NUM_POS_FIELDS];

  ros::Timer timer_;

  sensors::navState_t last_msg;
};

NUI_MAPPING_NS_FOOT

#endif // TEMPLATE_CLASS_H
