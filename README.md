# NUI Mapping Package
This package is designed to contain all the configurations, launch files, and LCM compatibility software for using the Norbit multibeam to map on NUI.

## Launch Files
All necessary process can be started using launch the following launch files and associated arguments

To run any of these launch files enter the following command on the vehicle:
```
roslaunch nui_mapping <launchfile> arg1:=<arg1_value> arg2:=<arg2_value>
```
### go_nui.launch
This is the top level launch file and will probably be the what you want to use for deployments.  This will start all other launch files.

**args:**

- **vessel_ns** [default: "nui"]:   this allows you to set the top level namespace of all your ros messages.  You really shouldn't need to set this to a non-default value
- **use_gridmap** [bool, default: false]:   do you want to run the gridmapper for  for this survey?
- **use_octomap** [bool, default: true]:  do you want to use the octomap for this survey?
- **norbit_ip** [string, REQUIRED]:  the ip address of your norbit.
- **file_prefix** [path, REQUIRED]:  specify the directory (absolute!, *not relative, no ~/ either*) where you want to log your data followed by the filename prefix.   example: `"/data/demo_survey"` will create the following :
    - rosbag file`/data/demo_survey_<timestamp>.bag` 
    - octomap file (if enabled) `/data/demo_survey_octomap.bt`  
    - gridmap file (if enabled) `/data/demo_survey_gridmap.bag`  

### lcm_translator.launch
This launches the lcm_bridge to generate ros tf and odometry messages from the NAVSTATE lcm topic
**args:**

 - **vessel_ns** [default: "nui"]:

### norbit.launch
this launches the norbit node according to the parameters specified in `config/norbit.yaml`
**args:**

 - **vessel_ns** [default: "nui"]
 - **norbit_ip** [string, REQUIRED]

### record.launch
This file starts the following processes

- rosbag:   records all topics not in the /viz/* namespace
- octomapper
- gridmapper

**args:**

- **use_gridmap** [bool, default: false]
- **use_octomap** [bool, default: true]
- **norbit_ip** [string, REQUIRED]
- **file_prefix** [path, REQUIRED]

## Configurations
All relevant configurations can be found in `nui_mapping/config`
### octomap.yaml
This config file allows you to adjust the parameters of the octomapper node.

- **viz_period** [float]:  the time interval between publishing the octomap for visualization.   Negative values turn off the visualization to save resources!
- **map** [dict]:
    - **res** [float]:  the  resolution  of  your  octomap  This  will  be  a  tradeoff  between  resolution  and  memory/processing  power
    - **frame_id** [string]:  the tf frame id you want your octomap in.   For this project this value should not be changed
    - **write_period** [float]:  How long (in seconds) between each time you cache to disk.


### norbit.yaml
- **sensor_frame** [string]:  the frame_id of your norbit.  This should match your urdf
- **pointcloud_topic** [string]:  the topic you want your messages to be published on.   This should not be changed since the octomapper expects the value to be "detections"
- **bathymetric_topic** [string]:  the topic you want to publish the native Norbit data to be published on
- **water_column_topic** [string]:  the topic for watercolum data.   put this in the /viz/* (e.g. "/viz/watercolumn") namespace if you don't want it to be logged.
- **cmd_timeout** [float]:  how long to wait for a response from a norbit command.   If commands aren't getting through because of network latency you can try to turn this up.
- **startup_settings and shutdown_settings** [dict]: these are the setting you want to apply when the norbit node is started/shutdown.  all the commands are of the form `cmd: "arguments"`.   For a full list of available commands see `norbit/doc/*DFD_external.pdf` section 7 command and control.  Below is a good example configuration
```
startup_settings:  #  the  norbit  settings  to  be  applied  at  startup  (see  doc/ *DFD_external.pdf)  
  set_power:  "1"  #  turn  norbit  on  at  startup
  set_gate_mode:  "0"  #mode  0=Gate  by  range  1=Gate  by  depth  2=Gate  by  range  and  depth
  set_range:  "1.0  60.0"
  set_time_source:  "1"  #  0:  IRIG-B  1:NTP  2:  IRIG-B  (inverted)  3:  NTP+PPS(pos)  4:  NTP+PPS(neg)  5:  ZDA+PPS(pos)  6:ZDA+PPS(neg)  7:ZDA  8:Free  run  (time  since  startup)
  set_rate:  "-1"  #  -1  for  automatic
  set_direction:  "0"  #  the  center  angle  of  the  swath
  set_opening_angle:  "160"  #  opening  angle  in  degrees
shutdown_settings:
  set_power:  "0"
```

### lcm_bridge.yaml
You probably won't need to change much here.  Should be pretty locked in to the vehicle configuration.

### gridmap.yaml

- **viz_period** [float]: see octomap viz_period
- **map** [dict]:
    - **len_x** [float]: the x dimension of your gridmap
    - **len_y** [float]:  if not specified len_x=len_y
    - **center_x/center_y** [float]: specified the center of your gridmap
    - **write_period** [float]: see octomap write_period

## Visualizing Real Time Data Topside

First you need to connect your laptop to the ros master.   Specify the following environmental variables for the terminal you are using (or edit your bashrc)

```
    export ROS_MASTER_URI=http://192.168.50.10:11311/
    export ROS_IP=<YOUR LAPTOP'S IP ADDRESS>
```

Now you can use tools like:

- `rostopic list` to show all published topics
- `rostopic echo <topic name>` to see what is being sent on that topic
- `rosrun rqt_topic rqt_topic` lcm_spy like interface for ros messages
- `rosrun rqt_tf_tree rqt_tf_tree` generates a graph of all TF links and shows how they are connected
- `roslaunch nui_mapping rviz.launch` this will open an RViz configured for the nui vehicle

# Norbit 

To set up a norbit on your vehicle you need to do a few things:  

- **Setup IP:**  You will need an fixed IP on your network.   I highly reccomend doing this by assigning the unit a fixed address via a DHCP server.   If you set the IP manually and are unable to recover it there is no way to reset it without sending it back to the manufacturer 
- **Install the Norbit GUI:**  The offical norbit gui is nice for changing parameters, debugging and setting up the unit.   You can download the .deb packages here: https://www.dropbox.com/sh/yyk4a5m2vg4uh3p/AABPwWsvwAWGN-X52UsZZWn1a?dl=0
- **Setup Time server:**  This is the one option that I can not reliably set using the ROS Norbit driver.  Therefore, you will need to open norbit gui and configure it there.  Make sure that you are getting valid timestamps in the lower left corner of the watercolum view!  [TODO:  find exact path to this menu]
- **Install ROS driver in your catkin_ws (jacuba_devel branch):** I have already done this for you but here is the link anyway.  https://bitbucket.org/croman_and_the_barbarians/norbit/src/jacuba_devel/
- **Configure the ROS driver:**  See section above!  

## Debugging:

- **Norbit data streaming in rostopic echo (or rqt_topic) but not displaying in RViz or Octomap**.   
    - This happens when the norbit time is incorrect!  Messages are being sent but with an invalid timestamp.  Therefore TF doesn't know what to do with them.  
    - **Actions to take:**  Open the Norbit gui and validate time server settings.   Make sure your timeserver is working and on the same network as the norbit. Use the Norbit GUI to make sure time is syncronized (lower left of the watercolum view).
    
# Topside and post processing

## CloudCompare

All tools and and output are designed to be compatible with CloudCompare.   This tool can be used to remove unwanted points, grid data, create messhes and make nice renders.   

If you don't have it already you can install it through the snap store!

## Octomap files

Each survey will save an octomapt (.bt) file to your logging directory.   This bt file can be converted to a PCD for visulization in CloudCompare

```
rosrun multibeam_process_tools octomap2pcd <octomap file> <output pcd file>
```

## Full bag processing

If you want to view the full bag you can use the patch_tester utility with the following launch command

```
roslaunch multibeam_process_tools patch_testerize.launch
```

This tool can be used to fuse the logged navigation and multibeam data into a PCD file for visualization and editing in CloudCompare.  A full example can be seen in this video:   https://youtu.be/2sBdvrAv_6E 






